import React from "react";
import { NavigationMenu } from "./navigation/nav-menu";
import { CardList } from "./catalog-card/card-list";
import { items } from "./items.json";

function App() {
  return (
    <div className="ui container">
      <NavigationMenu />
      <CardList items={items} />
    </div>
  );
}

export default App;
