import React from "react";

export const NavigationMenu = () => (
  <>
    <div className="ui secondary pointing menu container">
      <a className="active item" href="/">Home</a>
      <a className="item" href="/catalog">Catalog</a>
    </div>
  </>
);
