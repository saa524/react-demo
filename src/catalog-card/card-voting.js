import React, { useState } from "react";

export const CardVoting = ({ votes: originalVote }) => {
  const [votes, setVotes] = useState({ count: originalVote });

  const incrementVotes = () => {
    setVotes(prevState => {
      return { count: prevState.count + 1 };
    });
  };

  const decrementVotes = () => {
    setVotes(prevState => {
      return { count: prevState.count - 1 };
    });
  };

  return (
    <div style={{ width: "50px" }} className=" center aligned">
      <div className="left floated one wide column">
        <button onClick={incrementVotes} className="circular ui icon button">
          <i className="hand point up outline icon large"></i>
        </button>
      </div>
      <div className="left floated one wide column">
        <button onClick={decrementVotes} className="circular ui icon button">
          <i className="hand point down outline icon large"></i>
        </button>
      </div>
      <div className="center aligned one wide column">{votes.count}</div>
    </div>
  );
};
