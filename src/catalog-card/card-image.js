import React from "react";

export const CardImage = ({ image }) => (
  <div className="ui medium image">
    <img src={image.url} alt={image.alt} />
  </div>
);
