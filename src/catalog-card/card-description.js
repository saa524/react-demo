import React from 'react';

export const CardDescription = ({ benefits, buy }) => (
  <div className="description">
    <p>Benefits</p>
    <ol className="ui list">
      {benefits.map(ben => (
        <li value="-" key={ben}>{ben}</li>
      ))}
    </ol>
    <p>Buy when</p>
    <div>{buy}</div>
  </div>
);
