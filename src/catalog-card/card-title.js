import React from "react";

export const CardTitle = ({ title, price }) => (
  <>
    <div className="header">{title}</div>
    <div className="meta">
      <span className="price">{price}</span>
    </div>
  </>
);
