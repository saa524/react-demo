import React from "react";
import { CardDescription } from "./card-description";
import { CardTitle } from "./card-title";
import { CardImage } from "./card-image";
import { CardVoting } from "./card-voting";

export const CardContainer = ({ item }) => {
  const { title, benefits, price, buy, image, votes } = item;
  return (
    <div className="item">
      <CardVoting votes={votes} />
      <CardImage image={image} />
      <div className="middle aligned content">
        <CardTitle title={title} price={price} />
        <CardDescription benefits={benefits} buy={buy} />
      </div>
    </div>
  );
};
