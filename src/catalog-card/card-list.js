import React from "react";
import { CardContainer } from "./card-container";

export const CardList = ({ items }) => {
  return (
    <div className="ui divided items">
      {items.map(item => (
        <CardContainer item={item} key={item.title} />
      ))}
    </div>
  );
};
